\beamer@endinputifotherversion {3.10pt}
\select@language {english}
\beamer@sectionintoc {1}{Abstract}{2}{0}{1}
\beamer@sectionintoc {2}{Introduction}{5}{0}{2}
\beamer@sectionintoc {3}{Supporting Foveal and Peripheral Cones}{15}{0}{3}
\beamer@sectionintoc {4}{Sensory Surrogate}{32}{0}{4}
\beamer@sectionintoc {5}{Evaluation}{42}{0}{5}
\beamer@sectionintoc {6}{Further Issues}{68}{0}{6}
\beamer@sectionintoc {7}{Future Work}{71}{0}{7}
\beamer@sectionintoc {8}{Conclusions}{74}{0}{8}
\beamer@sectionintoc {9}{Acknowledgments}{75}{0}{9}
